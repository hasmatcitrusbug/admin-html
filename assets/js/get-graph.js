/*****  Graph 1  *****/

'use strict';
(function(factory) {
	if(typeof module === 'object' && module.exports) {
		module.exports = factory;
	} else {
		factory(Highcharts);
	}
}(function(Highcharts) {
	(function(H) {
		H.wrap(H.seriesTypes.column.prototype, 'translate', function(proceed) {
			const options = this.options;
			const topMargin = options.topMargin || 0;
			const bottomMargin = options.bottomMargin || 0;

			proceed.call(this);

			H.each(this.points, function(point) {
				if(options.borderRadiusTopLeft || options.borderRadiusTopRight || options.borderRadiusBottomRight || options.borderRadiusBottomLeft) {
					const w = point.shapeArgs.width;
					const h = point.shapeArgs.height;
					const x = point.shapeArgs.x;
					const y = point.shapeArgs.y;

					let radiusTopLeft = H.relativeLength(options.borderRadiusTopLeft || 0, w);
					let radiusTopRight = H.relativeLength(options.borderRadiusTopRight || 0, w);
					let radiusBottomRight = H.relativeLength(options.borderRadiusBottomRight || 0, w);
					let radiusBottomLeft = H.relativeLength(options.borderRadiusBottomLeft || 0, w);

					const maxR = Math.min(w, h) / 2

					radiusTopLeft = radiusTopLeft > maxR ? maxR : radiusTopLeft;
					radiusTopRight = radiusTopRight > maxR ? maxR : radiusTopRight;
					radiusBottomRight = radiusBottomRight > maxR ? maxR : radiusBottomRight;
					radiusBottomLeft = radiusBottomLeft > maxR ? maxR : radiusBottomLeft;

					point.dlBox = point.shapeArgs;

					point.shapeType = 'path';
					point.shapeArgs = {
						d: [
							'M', x + radiusTopLeft, y + topMargin,
							'L', x + w - radiusTopRight, y + topMargin,
							'C', x + w - radiusTopRight / 2, y, x + w, y + radiusTopRight / 2, x + w, y + radiusTopRight,
							'L', x + w, y + h - radiusBottomRight,
							'C', x + w, y + h - radiusBottomRight / 2, x + w - radiusBottomRight / 2, y + h, x + w - radiusBottomRight, y + h + bottomMargin,
							'L', x + radiusBottomLeft, y + h + bottomMargin,
							'C', x + radiusBottomLeft / 2, y + h, x, y + h - radiusBottomLeft / 2, x, y + h - radiusBottomLeft,
							'L', x, y + radiusTopLeft,
							'C', x, y + radiusTopLeft / 2, x + radiusTopLeft / 2, y, x + radiusTopLeft, y,
							'Z'
						]
					};
				}

			});
		});
	}(Highcharts));
}));

Highcharts.chart('container1', {
    data: {
        table: 'datatable1'
    },
    chart: {
        type: 'column',
        scrollablePlotArea: {
            minWidth: 400,
            scrollPositionX: 1
        }
	},
	colors: ['#002288', '#FCB414'],
    title: {
        text: ' '
	},
	legend: {
        // layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
		y: 0
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Units'
		}
	},
	plotOptions: {
		column: {
			borderRadiusTopLeft: 50,
            borderRadiusTopRight: 50,
            pointWidth : 20,
		}
	},
    tooltip: {
		backgroundColor: '#FFFFFF',
		// borderColor: '#002186',
		borderRadius: 2,
		borderWidth: 0.3,
		formatter: function () {
            return  '<div>' + this.point.name + '</div><br/>' +
			'<b>' + '<p>' + '$' + '</p>' + this.point.y + '</b>' + '</div>' ;
        }
        // formatter: function () {
        //     return '<b>' + this.series.name + '</b><br/>' +
        //         this.point.y + ' ' + this.point.name.toLowerCase();
        // }
    },
    responsive: {  
        rules: [{  
          condition: {  
            maxWidth: 500  
          },  
          chartOptions: {  
            legend: {  
              enabled: false  
            },
            plotOptions: {
                column: {
                    pointWidth : 7,
                }
            }, 
          }  
        }]  
    },
});

/***** End of Graph 1  *****/

/*****  Graph 2  *****/

Highcharts.chart('container2', {
    data: {
        table: 'datatable2'
    },
    chart: {
		type: 'column'
	},
	colors: ['#002288'],
    title: {
        text: ' '
	},
	legend: {
        // layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
		y: 0
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: ' '
		}
	},
	plotOptions: {
		column: {
			borderRadiusTopLeft: 50,
            borderRadiusTopRight: 50,
            pointWidth : 20
		}
	},
    tooltip: {
		backgroundColor: '#FFFFFF',
		// borderColor: '#002186',
		borderRadius: 2,
		borderWidth: 0.3,
        formatter: function () {
            return  '<div>' + this.point.name + '</div><br/>' +
			'<b>' + '<p>' + '$' + '</p>' + this.point.y + '</b>' + '</div>' ;
        }
    },
});

$(window).resize(function(){     

    if ($('.nav-header-div').width() <= 640 ){

        Highcharts.chart('container2', {
            data: {
                table: 'datatable2'
            },
            chart: {
                type: 'column'
            },
            colors: ['#002288'],
            title: {
                text: ' '
            },
            legend: {
                // layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                y: 0
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: ' '
                }
            },
            plotOptions: {
                column: {
                    borderRadiusTopLeft: 50,
                    borderRadiusTopRight: 50,
                    pointWidth : 7
                }
            },
            tooltip: {
                backgroundColor: '#FFFFFF',
                // borderColor: '#002186',
                borderRadius: 2,
                borderWidth: 0.3,
                formatter: function () {
                    return  '<div>' + this.point.name + '</div><br/>' +
                    '<b>' + '<p>' + '$' + '</p>' + this.point.y + '</b>' + '</div>' ;
                }
            },
        });
        
    }

});

/***** End of Graph 2  *****/

/***** Graph 3  *****/

// Build the chart
Highcharts.chart('container31', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ' '
    },
    tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: false,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: false
        }
	},
	
    series: [{
        data: [{
            name: ' Other ',
            y: 60,
            sliced: false,
			selected: false,	
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#CFCFCF'], // start
				[1, '#CFCFCF'] // end
				]
			},
			
		}, 
		{
            name: ' Monthly Memberships ',
			y: 40,
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#FEDB2D'], // start
				[1, '#FCB414'] // end
				]
			}
        }]
    }]
});

// Build the chart
Highcharts.chart('container32', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ' '
    },
    tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: false,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: false
        }
	},
	
    series: [{

        data: [
			{
            name: ' Other ',
            y: 20,
            sliced: false,
			selected: false,	
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#CFCFCF'], // start
				[1, '#CFCFCF'] // end
				]
			}
		}, 
		{
            name: ' Monthly Memberships ',
			y: 40,
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#EA0000'], // start
				[1, '#D10000'] // end
				]
			}
        },
			{
            name: ' Other ',
            y: 40,
            sliced: false,
			selected: false,	
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#CFCFCF'], // start
				[1, '#CFCFCF'] // end
				]
			}
			
		}, 
		]
    }]
});

Highcharts.chart('container33', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ' '
    },
    tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: false,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: false
        }
	},
	
    series: [{

        data: [
			{
            name: ' Other ',
            y: 20,
            sliced: false,
			selected: false,	
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#0045BB'], // start
				[1, '#002186'] // end
				]
			}
		}, 
		{
            name: ' Monthly Memberships ',
			y: 80,
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#CFCFCF'], // start
				[1, '#CFCFCF'] // end
				]
			}
        },
			
		]
    }]
});

/***** End of Graph 3  *****/

/***** Graph 4  *****/

Highcharts.chart('container4', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
		type: 'variablepie'
    },
    title: {
        text: ' '
    },
    tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true
            },
            showInLegend: false
        }
	},
	colors: ['#FFC20F', '#002186', '#D10000',],
    series: [{
        minPointSize: 10,
        innerSize: '70%',
        zMin: 0,
		name: 'countries',
		data: [{
            name: 'Monthly',
			y: 40000,
			z: 80,
			descriptiveTooltip : 'Descriptive paragraph of context'
        }, {
            name: '12 Month',
            y: 40000,
            z: 40
        }, {
            name: '6 Month',
            y: 20000,
            z: 20
        }]
    }]
});

/***** End of Graph 4  *****/

/***** Graph 5  *****/

Highcharts.chart('container5', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: ' '
    },
    legend: {
        align: 'left',
        verticalAlign: 'top',
		y: 0
    },
	colors: ['#002186', '#FCB414'],
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
			'Jul',
			'Aug',
			'Sep',
			'Oct',
			'Nov',
            'Dec'
        ],
    },
    yAxis: {
        title: {
            text: ' '
        }
    },
    tooltip: {
        shared: true,
        // valueSuffix: '$'
    },
	credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.1
        }
    },
    series: [{
        name: '2019',
        data: [3, 4, 3, 5, 4, 10, 12, 30, 4, 3, 5, 4]
    },
	 {
        name: '2020',
        data: [1, 3, 4, 3, 3, 5, 24, 13, 89, 23, 55, 44]
    }]
});

/***** End of Graph 5  *****/

/***** Graph 6  *****/

Highcharts.chart('container6', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ' '
    },
    tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: false,
            cursor: 'pointer',
            dataLabels: {
                enabled: true
            },
            showInLegend: false
        }
	},
	/*
	legend: {
		backgroundColor: '#fff',
		borderColor: '#e2e2e2',
		borderWidth: 1
	}, */
    series: [
		{
		// showInLegend: true,
        data: [
		 	{
            name: ' Upcoming',
			y: 20,
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#FEDB2D'], // start
				[1, '#FCB414'] // end
				]
			}
        },
			{
            name: ' Scheduled Classes',
            y: 40,
            sliced: false,
			selected: false,	
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#0045BB'], // start
				[1, '#002186'] // end
				]
			}
		}, 
		{
            name: ' Completed',
			y: 40,
			color: {
			linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
			stops: [
				[0, '#EA0000'], // start
				[1, '#D10000'] // end
				]
			}
        }]
    }]
});

/***** End of Graph 6  *****/