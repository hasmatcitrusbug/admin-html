

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
})


$(function() {
  $('#class_type').change(function(){
    $('.colors').hide();
    $('#' + $(this).val()).show();
  }); 

  $('#Payment_type').change(function(){ 
    $('.colors2').hide();
    $('#' + $(this).val()).show();
  });

  $('#billing_type').change(function(){
    $('.colors3').hide();
    $('#' + $(this).val()).show();
  });

  $('#payout_method_type').change(function(){
    $('.colors4').hide();
    $('#' + $(this).val()).show();
  }); 

  $('#timesheet_method_type').change(function(){
    // $('.hourly-view-filled').hide();
    if ($(this).val() == 'revenue-share-opt1') { 
      
      $('#hours, #pay_rate, #payout').addClass('hidden-value');
      $('#hours, #pay_rate, #payout').removeClass('show-value');
      $('.rev-view-filled').addClass('show-value');

      $('.inner-hourly').addClass('hidden-value');
      $('.inner-hourly').removeClass('show-value');
      $('.inner-default').addClass('show-value');
    } 
    else {
      /*$('#hours, #pay_rate, #payout').addClass('show-value');
      $('#rev_share, #hourly').addClass('hidden-value');*/

      $('#hours, #pay_rate, #payout').addClass('show-value');
      $('.rev-view-filled').removeClass('show-value');

      $('.inner-hourly').addClass('show-value');
      $('.inner-default').addClass('hidden-value');
      $('.inner-default').removeClass('show-value');
    }
  }); 

  
  /*
  $('#timesheet_method_type').change(function(){
    if ($(this).val() == 'hourly-opt2') {
      $('#hours, #pay_rate, #payout').removeClass('hourly-view-filled');
      $('#rev_share, #hourly').addClass('hourly-view-filled');
      $('.inner-hourly').addClass('show-value');
      $('.inner-default').addClass('hidden-value');
      $('.inner-default').removeClass('show-value');

    } 
    else {
      $('#hours, #pay_rate, #payout').addClass('hourly-view-filled');
      $('#rev_share, #hourly').removeClass('hourly-view-filled');
     
      $('.inner-hourly').addClass('hidden-value');
      $('.inner-hourly').removeClass('show-value');
      $('.inner-default').addClass('show-value');
    }
  }); */

  $('#payout_method_type').change(function(){
    if($(this).val() == 'revenue-share-option'){ 
        $( ".invoicedaccount" ).addClass( "facility-show" );
        $( ".parentpaid" ).removeClass( "facility-show" );
    
    }
    else if($(this).val() == 'hourly-option')
    {
        $( ".invoicedaccount" ).removeClass( "facility-show" );
        $( ".parentpaid" ).addClass( "facility-show" );
    }
    else
    {
      $( ".parentpaid" ).removeClass( "facility-show" );
    }
  });

  $('#Payment_type').change(function(){
    if($(this).val() == 'invoiced_account'){ 
        $( ".invoicedaccount" ).addClass( "facility-show" );
        $( ".parentpaid" ).removeClass( "facility-show" );
        $( "#halt_date" ).hide();
    }
    else if($(this).val() == 'parent_paid')
    {
        $( ".invoicedaccount" ).removeClass( "facility-show" );
        $( ".parentpaid" ).addClass( "facility-show" );
        $( "#halt_date" ).show();
    }
    else
    {
      $( ".parentpaid" ).removeClass( "facility-show" );
      $( "#halt_date" ).show(); 
    }
  });

  
  $(document).ready(function(){
    $(".linkround-blk").click(function(){
      $("#"+$(this).attr("data-id")).addClass("click-change-icon");
    });

    $(".cancel-both-popup").click(function(){
      $('#add-calendar-event').modal('hide');
    });
    

  });

});
  
$(document).ready(function () {
  autoPlayYouTubeModal();
});

$('.multiple-select-date').datepicker({
  multidate: true,
  format: 'dd-mm-yyyy'
});

var array = ["2018-02-14","2018-01-15","2013-01-26"]
$('.redcolor-select-option').datepicker({
  multidate: true,
  datesDisabled: ['01/02/2019', '02/21/2019','01/14/2017']
  
});

/***  redcolor */

/*** end of redcolor */

function autoPlayYouTubeModal() {
      var trigger = $('.videoModalTriger');
  trigger.click(function () {
      var theModal = $(this).data("target");
      var videoSRC = $(this).attr("data-videoModal");
      var videoSRCauto = videoSRC + "?autoplay=1";
      $(theModal + ' iframe').attr('src', videoSRCauto);
      $(theModal).on('hidden.bs.modal', function (e) {
          $(theModal + ' iframe').attr('src', '');
      });
  });
};

$(document).ready(function(){  
  $(".sidebar-mobile-main-toggle").click(function(){
    //$('#navbar-mobile').hide();
    $('.nav-mobile').addClass('collapsed');
    $('.navbar-collapse.collapse').removeClass('in');
    
  });
  $(".open-unavailable-modal").click(function(){
    $("#updateevent-calendar-event").modal('show');
    $("#more-option-modal").modal('hide');
  });  
});

// .open-unavailable-modal




$(document).ready(function () {
  $('#passcheckbox1').change(function () {
    $('.pass-none').fadeToggle();
  });
});


$(document).ready(function() {

  // $('.fc-resourceTimelineMonth-button').click(function(){
  //   $(".viewby-calender").hide();
  //   alert("helolo");
  // });

  $(".fc-resourceTimelineMonth-button").on("click", function(){
    // $(".viewby-calender").hide();
    $(".viewby-calender").addClass("hide-class");
  });

  

  // $("#txtEditor").Editor();
  // $("#txtEditor2").Editor();
  // $("#txtEditor3").Editor();
  // $(".Editor-editor").html("<p class='defalut_txt1' style='display:none;'>Please do not schedule me for an event during this time I am unavailable to work.</p>"); 

  // 

  /*$(".hover_rotation_modal").hover( function(){
    $("#sport_rotation").modal('show').fadeIn(300);
    $("#add-data-modal1").modal('hide');
  });*/

});

/****   checkbox select in DELETE A SESSION Popup   ****/

$(".checkbox_select_box").click(function() {
  
  if($(this).is(":checked")) {
    $(".delete-refund-btnshow").show();
    $(".delete-sbutton").hide();
  } 
  else {
    $(".delete-sbutton").show();
    $(".delete-refund-btnshow").hide();
  }

});

// $('#yes_no_toggle').on("click", function(){
//   $("#reserve_accounts_modal").modal('show');
//   $("#view-leads-details").modal('hide');
// });

$("#yes_no_toggle").on("click", function(){
  $("#reserve_accounts_modal").modal('show');
  $("#view-leads-details").modal('hide');
});
$(".close_cancel_viewlead").click(function(){
  $(".btn-toggle-yesno").removeClass("active");
});

 
// $(".datepicker-2").bind('click', function() {
//     if(!$(".dropdown-menu").hasClass('add_cal')) {
//       $(".datepicker-orient-top").addClass('add_cal');
//     }
//     else {
//         $(this).removeClass('remove_cal');
//     }
//   });

/********/

/**********
  $('#colorpalettediv').colorPalettePicker({
    bootstrap: 3,
    lines: 4
  });
**********/

/*****
$(document).ready(function(){
  $("#navbar-mobile").click(function(){
    $(".sidebar-mobile-main .sidebar-div").hide();    
    $(".navbar-top").removeClass('sidebar-mobile-main');
  });
  $(".sidebar-mobile-main-toggle").click(function(){
    $("#navbar-mobile").hide();
  });

});
******/

$("#past-report-date").click(function(){
	$(".form-group.form-datpicker-ranges").hide();
})

$("#time-sheet-report-date").click(function(){
  $(".form-group.form-datpicker-ranges").hide();
  
    $(".btn-run").remove();

    $(".start-date-label").parent().prepend('<a href="#" class="btn btn-run" >Run Current Report</a>');

    $(".applyBtn").html('Run Report')
})

$(window).load(function() {
  $(".fc-left").parent().prepend('<div class="fc-left-one"></div>');
  $(".fc-sun").parent().prepend('<td class="fc-left-1 fc-widget-content fc-sun fc-past"></td>');
  // $(".calendar-left-data").parent().prepend('<td class="">test</td>');
  $(".fc-sun").parent().first().prepend('<td class="fc-left-2">Text</td>');

  // $(".fc-cell-text").find('Resources'.html).hide();

  $(".fc-cell-text:first").css("display", "none");
  // $(".fc-cell-text:first").parent().prepend('<div class="view-by-calender"><span>View By</span> <select class="custom-select" data-width="100%"> <option>Orelance</option> <option>Bostan</option> <option>New Yourk</option> </select></div>');
});

$('.fc-resourceTimelineDay-button').click(function(){
  $(".fc-cell-text").addClass('hide-text');
});

$('.fc-resourceTimelineWeek-button').click(function(){
  $(".fc-cell-text").addClass('hide-text');
});


$('.fc-resourceTimelineMonth-button').click(function(){
  $(".fc-cell-text").addClass('hide-text');
});




$('#edit-class').click(function(){
  $(".form-view").hide();
  $(".edit-view").show();
});

$('#save-update').click(function(){
  $(".form-view").show();
  $(".edit-view").hide();
});

$('#slide-calendar').on('shown.bs.modal', function () {
  $(".fullcalendar-event-colors").fullCalendar('render');
});





// $(document).ready(function() {
  

  
  // $(".fc-agendaWeek-button").click(function(){
  //   $(".fc-minor").next().prepend(' <td class="g">test</td> ');
  // });
  

// });

// document.addEventListener('DOMContentLoaded', function() {
//   var calendarEl = document.getElementById('calendar');

//   var calendar = new FullCalendar.Calendar(calendarEl, {
//     plugins: [ 'interaction', 'resourceTimeline', 'timeGrid' ],
//     selectable: true,
//     defaultView: 'resourceTimelineWeek',
//     header: {
//       left: 'prev,next today',
//       center: 'title',
//       right: 'resourceTimelineWeek,timeGridDay'
//     },
//     resources: [
//       { id: 'a', title: 'Room A' },
//       { id: 'b', title: 'Room B' },
//       { id: 'c', title: 'Room C' }
//     ],
//     dateClick: function(info) {
//       alert('clicked ' + info.dateStr + ' on resource ' + info.resource.id);
//     },
//     select: function(info) {
//       alert('selected ' + info.startStr + ' to ' + info.endStr + ' on resource ' + info.resource.id);
//     }
//   });

//   calendar.render();
// });

 


// custom dropdown
//custom dropdown end


